import React from 'react';
import './App.css';
import Content from "./components/content";
// import * as axios from 'axios';


class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alphabet: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

        };
    }


    getCookieValueByName (name) {
        return !!document.cookie.split(";")
            && !!document.cookie.split(";").map(i => i.trim()).filter(i => i.startsWith(name))[0]
            && document.cookie.split(";").map(i => i.trim()).filter(i => i.startsWith(name))[0].split("=")[1];
    }


    render() {

        return (
            <div className="App">
                <Content
                    props = {{
                        alphabet: this.state.alphabet,
                        shuffleAlphabet: [...this.state.alphabet].sort(() => {
                            return Math.random() - 0.5;
                        }),
                    }}
                />

            </div>
        );
    }
}

export default App;
