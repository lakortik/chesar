import React, {useState} from 'react';

function Chesar(
    {props: {
        alphabet,
        setInfo,
        hide,
        downloadAsFile
    }},
) {

    const [alphabetWithStep, setAlphabetWithStep] = useState(alphabet);
    const [textFromFile, setTextFromFile] = useState();
    const [fileName, setFileName] = useState();

    const calcAlphabetWithStep = (step) => {

        let newAlphabetWithStep = [...alphabet];
            setAlphabetWithStep(newAlphabetWithStep);

            for (let i = 0; i < step; i++) {
                newAlphabetWithStep.push(newAlphabetWithStep.shift());

                setAlphabetWithStep(newAlphabetWithStep);
            }
    };


    return !hide && (

        <div>
            <label htmlFor="exampleFormControlSelect1">Укажите сдвиг (N)</label>
            <select className="form-control w250px" id="exampleFormControlSelect1"
                    onChange={() => {
                        calcAlphabetWithStep(document.getElementById('exampleFormControlSelect1').value);
                    }}
            >
                {alphabet.map((i,k) => <option
                        key={k}
                    >
                        {k}
                    </option>
                )}
            </select>

            <label htmlFor="exampleFormControlFile1">Прикрепите файл</label>
            <input type="file" className="form-control-file  mb-2" id="exampleFormControlFile1"
                onChange={
                    () => {
                        const file = document.getElementById("exampleFormControlFile1").files[0];

                        setFileName(file.name);

                        const reader = new FileReader();
                        reader.readAsText(file, "UTF-8");
                        reader.onload = (evt) => {
                            setTextFromFile(evt.target.result);
                        };
                        reader.onerror = (evt) => {
                            setInfo("Ошибка чтения файла");
                        }

                    }
                }
            />
            <button type="button" className="btn btn-primary  p-2"
                onClick={() => {
                    const newText = textFromFile.split('')
                    .map(symbolNeedReplace => alphabet.includes(symbolNeedReplace)
                        ? alphabetWithStep[alphabet.findIndex(symbol => symbolNeedReplace === symbol)]
                        : symbolNeedReplace
                    )
                    .join('');

                    setInfo(newText);
                    downloadAsFile(newText, `encС_${fileName}`);
                }}
            >
                Зашифровать
            </button>

            <button type="button" className="btn btn-success m-2"
                onClick={() => {
                    const newText = textFromFile.split('')
                        .map(symbolNeedReplace => alphabet.includes(symbolNeedReplace)
                            ? alphabet[alphabetWithStep.findIndex(symbol => symbolNeedReplace === symbol)]
                            : symbolNeedReplace
                        )
                        .join('');

                    setInfo(newText);
                    downloadAsFile(newText, `decС_${fileName}`);

                }}
            >

                Расшифровать
            </button>



        </div>

    );
}

export default Chesar;