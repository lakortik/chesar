import React, {useState, useEffect} from 'react';

function Viziner(
    {props: {
        setInfo,
        hide,
        downloadAsFile,
        shuffleAlphabet
    }},
) {


    const [key, setKey] = useState('');
    const [square, setSquare] = useState({});
    const [textFromFile, setTextFromFile] = useState();
    const [fileName, setFileName] = useState();

    useEffect (()=>{
        let newSquare = {...square};
        let copyShuffleAlphabetForStep = [...shuffleAlphabet];

        key.split('')
        .forEach((keySymbol) => {

            if (newSquare[keySymbol] !== undefined){return};

            while (copyShuffleAlphabetForStep[0] !== keySymbol){
                copyShuffleAlphabetForStep.push(copyShuffleAlphabetForStep.shift());
            }

            newSquare[keySymbol] = copyShuffleAlphabetForStep;
        });

        setSquare(newSquare);
    },[key]);


    return !hide && (

        <div>
            <label htmlFor="key-input">Введите ключ</label>

            <input className="form-control" type="text" placeholder="Введите ключ шифрования" id='key-input'
                value={key}
                onChange={() => setKey(document.getElementById('key-input').value.toUpperCase()
                    .replace(' ','')
                    .replace('1','')
                    .replace('2','')
                    .replace('3','')
                    .replace('4','')
                    .replace('5','')
                    .replace('6','')
                    .replace('7','')
                    .replace('8','')
                    .replace('9','')
                    .replace('0','')
                )}
            />

            <br/>

            <label htmlFor="exampleFormControlFile1">Прикрепите файл</label>
            <input type="file" className="form-control-file  mb-2" id="exampleFormControlFile1"
                onChange={
                    () => {
                        const file = document.getElementById("exampleFormControlFile1").files[0];

                        setFileName(file.name);

                        const reader = new FileReader();
                        reader.readAsText(file, "UTF-8");
                        reader.onload = (evt) => {
                            setTextFromFile(evt.target.result);
                        };
                        reader.onerror = (evt) => {
                            setInfo("Ошибка чтения файла");
                        }

                    }
                }
            />
            <button type="button" className="btn btn-primary  p-2"
                onClick={() => {
                    let newTextSplit = textFromFile.split('');

                    //=== удлиннили ключ до длинны текста
                    const repeatCount = (newTextSplit.length / key.length).toFixed(0) +1;
                    let longKey = [];
                    for (let i = 0; i < repeatCount; i++) {
                        longKey = longKey.concat(key.split(''))
                    }
                    longKey.length = newTextSplit.length;
                    //===


                    const newText = newTextSplit
                    .map((symbolNeedReplace, index) => shuffleAlphabet.includes(symbolNeedReplace)
                        ? square[longKey[index]][shuffleAlphabet.findIndex(symbol => symbolNeedReplace === symbol)]
                        : symbolNeedReplace
                    )
                    .join('');

                    setInfo(newText);
                    downloadAsFile(newText, `encV_${fileName}`);
                }}
            >
                Зашифровать
            </button>

            <button type="button" className="btn btn-success m-2"
                onClick={() => {
                    let newTextSplit = textFromFile.split('');

                    //=== удлиннили ключ до длинны текста
                    const repeatCount = (newTextSplit.length / key.length).toFixed(0) +1;
                    let longKey = [];
                    for (let i = 0; i < repeatCount; i++) {
                        longKey = longKey.concat(key.split(''))
                    }
                    longKey.length = newTextSplit.length;
                    //===


                    const newText = newTextSplit
                        .map((symbolNeedReplace, index) => shuffleAlphabet.includes(symbolNeedReplace)
                            ? shuffleAlphabet[square[longKey[index]].findIndex(symbol => symbolNeedReplace === symbol)]
                            : symbolNeedReplace
                        )
                        .join('');

                    setInfo(newText);
                    downloadAsFile(newText, `decV_${fileName}`);

                }}
            >

                Расшифровать
            </button>

            <hr/>

            <table className='table table-bordered table-dark'>
                <thead>
                    <tr className=''>
                        <th className='w40px'></th>
                        {shuffleAlphabet.map((i) => <th
                                className={'p-2 w40px'}
                                key={i}
                            >
                                {i}
                            </th>
                        )}
                    </tr>
                </thead>
                <tbody>
                {
                    key.split('')
                    .map((keySymbol, index) => {

                        return <tr className='' key={index}>
                            <td className='w40px'> {keySymbol}</td>
                            {(square[keySymbol] || []).map((i) => <td
                                    className={'p-2 w40px'}
                                    key={i}
                                >
                                    {i}
                                </td>
                            )}
                        </tr>

                    })

                }

                </tbody>
            </table>
            <hr/>




        </div>

    );
}

export default Viziner;