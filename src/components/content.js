//Modal component
import React, {useState, useEffect} from 'react';
import Chesar from '../components/chesar';
import Viziner from '../components/viziner';

function Content(
    {props: {
        alphabet,
        shuffleAlphabet,
    }},
) {

    const [currentContentType, setCurrentContentType] = useState(0);
    const [headText, setHeadText] = useState('Выберите метод');
    const [info, setInfo] = useState('');

    useEffect (()=>{
        setInfo('');

        if (currentContentType === 1){setHeadText('Метод Цезаря')}
        if (currentContentType === 2){
            setHeadText('Метод Виженера');
            setInfo('Внимание! При перезагрузке страницы случайным образом генерируется алфавит замены. ' +
                'т.е. для успешной шифровки и дешифровки не нужно перезагружать страницу.'
            );
        }

    },[currentContentType]);


    const downloadAsFile = (data, name) => {
        let a = document.createElement("a");
        let file = new Blob([data], {type: 'application/json'});
        a.href = URL.createObjectURL(file);
        a.target = '_blank';
        a.download = name;
        a.click();
    };


    return (

        <div className={'content p-2'}>
            <h2>{headText}</h2>

            <div className="form-check">
                <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1"
                       value='1'
                       onClick={() => setCurrentContentType(1)}
                />
                <label className="form-check-label" htmlFor="exampleRadios1">
                    Метод Цезаря
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2"
                       value='2'
                       onClick={() => setCurrentContentType(2)}
                />
                <label className="form-check-label" htmlFor="exampleRadios2">
                    Метод Виженера
                </label>
            </div>

            <br/>

            {
                <>
                    <Chesar props = {{
                            hide: currentContentType !== 1,
                            setInfo,
                            alphabet,
                            downloadAsFile,
                        }}
                    />

                    <Viziner props = {{
                            hide: currentContentType !== 2,
                            setInfo,
                            downloadAsFile,
                            shuffleAlphabet,
                        }}
                    />
                </>
            }

            <br/>
            <br/>

            <h2>Сообщение программы:</h2>
            <p>{info}</p>


        </div>



    );
}

export default Content;